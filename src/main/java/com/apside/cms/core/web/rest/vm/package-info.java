/**
 * View Models used by Spring MVC REST controllers.
 */
package com.apside.cms.core.web.rest.vm;
