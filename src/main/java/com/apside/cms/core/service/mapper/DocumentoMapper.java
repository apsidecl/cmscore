package com.apside.cms.core.service.mapper;

import com.apside.cms.core.domain.*;
import com.apside.cms.core.service.dto.DocumentoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Documento and its DTO DocumentoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DocumentoMapper extends EntityMapper<DocumentoDTO, Documento> {



    default Documento fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documento documento = new Documento();
        documento.setId(id);
        return documento;
    }
}
