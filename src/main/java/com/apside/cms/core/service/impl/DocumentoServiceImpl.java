package com.apside.cms.core.service.impl;

import com.apside.cms.core.service.DocumentoService;
import com.apside.cms.core.domain.Documento;
import com.apside.cms.core.repository.DocumentoRepository;
import com.apside.cms.core.service.dto.DocumentoDTO;
import com.apside.cms.core.service.mapper.DocumentoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Documento.
 */
@Service
@Transactional
public class DocumentoServiceImpl implements DocumentoService {

    private final Logger log = LoggerFactory.getLogger(DocumentoServiceImpl.class);

    private final DocumentoRepository documentoRepository;

    private final DocumentoMapper documentoMapper;

    public DocumentoServiceImpl(DocumentoRepository documentoRepository, DocumentoMapper documentoMapper) {
        this.documentoRepository = documentoRepository;
        this.documentoMapper = documentoMapper;
    }

    /**
     * Save a documento.
     *
     * @param documentoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DocumentoDTO save(DocumentoDTO documentoDTO) {
        log.debug("Request to save Documento : {}", documentoDTO);
        Documento documento = documentoMapper.toEntity(documentoDTO);
        documento = documentoRepository.save(documento);
        return documentoMapper.toDto(documento);
    }

    /**
     * Get all the documentos.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DocumentoDTO> findAll() {
        log.debug("Request to get all Documentos");
        return documentoRepository.findAll().stream()
            .map(documentoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one documento by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DocumentoDTO findOne(Long id) {
        log.debug("Request to get Documento : {}", id);
        Documento documento = documentoRepository.findOne(id);
        return documentoMapper.toDto(documento);
    }

    /**
     * Delete the documento by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Documento : {}", id);
        documentoRepository.delete(id);
    }
}
